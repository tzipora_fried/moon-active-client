
import http from "../Common/Common";

const getAll = () => {
    return http.get("/");
};

const get = id => {
    return http.get(`/${id}`);
};

const create = data => {
    return http.post("/create", data);
};

const update = (id, data) => {
    return http.put(`/${id}`, data);
};

const remove = id => {
    return http.delete(`/${id}`);
};

const removeAll = () => {
    return http.delete(`/promotions`);
};

const findByTitle = title => {
    return http.get(`/promotions?title=${title}`);
};

const getRemotePromotions = (queryParam) => {
    return http.get(`/paginate${queryParam}`);
};

export default {
    getAll,
    get,
    create,
    update,
    remove,
    removeAll,
    findByTitle,
    getRemotePromotions
};