import React, {useState, useReducer, useEffect} from 'react';
import Paper from '@material-ui/core/Paper';
import {
    DataTypeProvider, EditingState, SelectionState,
    VirtualTableState,

} from '@devexpress/dx-react-grid';
import {
    Grid,
    VirtualTable,
    TableHeaderRow, TableSelection, TableEditRow, TableEditColumn,
} from '@devexpress/dx-react-grid-material-ui';
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import moment from "moment";
import PromotionDataService from "../../services/PromotionService";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import './VirtualTablePromotion.css';

const VIRTUAL_PAGE_SIZE = 100;
const MAX_ROWS = 10000
const getRowId = row => row._id;
const buildQueryString = (skip, take) => `?skip=${skip}&take=${take}`;

const initialState = {
    rows: [],
    skip: 0,
    requestedSkip: 0,
    take: VIRTUAL_PAGE_SIZE,
    totalCount: 0,
    loading: false,
    lastQuery: ''
};

function reducer(state, {type, payload}) {
    switch (type) {
        case 'UPDATE_ROWS':
            return {
                ...state,
                ...payload,
                loading: false,
            };
        case 'START_LOADING':
            return {
                ...state,
                ...payload
            };
        case 'REQUEST_ERROR':
            return {
                ...state,
                loading: false,
            };
        case 'FETCH_INIT':
            return {
                ...state,
                loading: true,
            };
        case 'UPDATE_QUERY':
            return {
                ...state,
                lastQuery: payload,
            };
        case 'DELETE_ROW':
            return {
                ...state,
                rows: state.rows.filter(function (row) {
                    return row._id !== payload;
                })
            }
        default:
            return state;
    }
}

export default function VirtualTablePromotion() {
    const [state, dispatch] = useReducer(reducer, initialState);
    const columns = [
        {name: 'name', title: 'Name', getCellValue: row => row.name},
        {name: 'type', title: 'Type', getCellValue: row => row.type},
        {name: 'startDate', title: 'Start Date', getCellValue: row => row.startDate},
        {name: 'endDate', title: 'End Date', getCellValue: row => row.endDate},
        {name: 'userGroupName', title: 'User Group Name', getCellValue: row => row.userGroupName},
        {name: 'Action', title: 'Action'},
    ];
    const tableColumnExtensions = [
        {columnName: 'name', width: 150},
        {columnName: 'type', width: 100},
        {columnName: 'startDate', width: 150},
        {columnName: 'endDate', width: 150},
        {columnName: 'userGroupName', width: 200},
        {name: 'Action', title: 'Action', width: 50},
    ];
    const editingStateColumnExtensions = [
        {columnName: 'Action', editingEnabled: false},
    ];
    const [openAlert, setOpenAlert] = useState(false);
    const [selection, setSelection] = useState([]);
    const getRemoteRows = (requestedSkip, take) => {
        dispatch({type: 'START_LOADING', payload: {requestedSkip, take}});
    };
    const loadData = async () => {
        const {
            requestedSkip, take, loading, lastQuery
        } = state;
        const query = buildQueryString(requestedSkip, take);
        if (query !== lastQuery && !loading) {
            dispatch({type: 'FETCH_INIT'});
            PromotionDataService.getRemotePromotions(query)
                .then(response => {
                    dispatch({
                        type: 'UPDATE_ROWS',
                        payload: {
                            skip: requestedSkip,
                            rows: response.data.docs,
                            totalCount: response.data.totalDocs,
                        },
                    });
                }).catch(() => dispatch({type: "REQUEST_ERROR"}));
            dispatch({ type: 'UPDATE_QUERY', payload: query });
        }
    };

    useEffect(() => loadData());

    const handleClose = (event, reason) => {
        setOpenAlert(false);
    };

    function deletePromotion() {
        let selectedRowId = selection[0];
        PromotionDataService.remove(selectedRowId).then(({data}) => {
            dispatch({type: 'DELETE_ROW', payload: selectedRowId});
            setSelection([]);
        });
    }

    function duplicatePromotion() {
        let rowToDuplicate = rows.find(function (item) {
            return item._id === selection[0];
        });
        const { _id,...rowToDuplicateNoId} = rowToDuplicate
        PromotionDataService.create(rowToDuplicateNoId);
        setSelection([]);
    }

    const onValueChange = (event) => {
        if(selection.length === 0){
            setOpenAlert(true);
            return;
        }
        switch (event.target.value) {
            case "Delete": {
                deletePromotion();
                break;
            }
            case "Duplicate": {
                duplicatePromotion();
                break;
            }
        }
    }
    const SelectDataType = () => (
        <Select
            onChange={onValueChange}
            style={{width: '100%'}}>
            <MenuItem value="Delete">
                Delete
            </MenuItem>
            <MenuItem value="Duplicate">
                Duplicate
            </MenuItem>
        </Select>
    );

    const SelectTypeProvider = props => (
        <DataTypeProvider
            disabled={true}
            formatterComponent={SelectDataType}
            {...props}
        />
    );

    const DateFormatter = ({value}) => {
        return moment(value).format('D/MM/YYYY')
    };

    const DateTypeProvider = props => (
        <DataTypeProvider
            formatterComponent={DateFormatter}
            {...props}
        />
    );
    const commitChanges = ({ added, changed, deleted }) => {
        const {
            requestedSkip, take, totalCount
        } = state;
        if (added) {
            PromotionDataService.create(added[0]);
            // todo support in case user in the last page
            if (requestedSkip + take >= totalCount) {

            }
        }
        if (changed) {
            const currentRow = rows.find((row) => changed[row._id]);
            const editedRow = {...currentRow, ...changed[currentRow._id]};
            let changedRows = rows.map((row) =>
                changed[row._id] ? { ...row, ...changed[row._id] } : row
            );
            dispatch({
                type: 'UPDATE_ROWS',
                payload: {
                    skip: requestedSkip,
                    rows: changedRows
                },
            })
            PromotionDataService.update(editedRow._id,editedRow);
        }
    };

    const {
        rows, skip, totalCount, loading,
    } = state;

    return (
        <Paper>
            <Grid
                rows={rows}
                columns={columns}
                getRowId={getRowId}>

                <SelectionState
                    selection={selection}
                    onSelectionChange={setSelection}
                />
                <SelectTypeProvider
                    for={['Action']}
                />

                <EditingState
                    onCommitChanges={commitChanges}
                    columnExtensions ={editingStateColumnExtensions}
                />
                <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleClose}
                          anchorOrigin={{vertical: 'top', horizontal: 'center'}}>
                    <Alert onClose={handleClose} severity="error">
                        No selected row
                    </Alert>
                </Snackbar>
                <DateTypeProvider
                for={['startDate']}/>

                <DateTypeProvider
                    for={['endDate']}/>

                <VirtualTableState
                    loading={loading}
                    totalRowCount={totalCount}
                    pageSize={VIRTUAL_PAGE_SIZE}
                    skip={skip}
                    getRows={getRemoteRows}/>

                <VirtualTable columnExtensions={tableColumnExtensions} height={700}/>

                <TableSelection highlightRow={true}/>

                <TableHeaderRow/>

                <TableEditRow/>

                <TableEditColumn
                    showAddCommand
                    showEditCommand
                />

            </Grid>
        </Paper>
    );
};
