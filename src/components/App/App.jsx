import React, {useState} from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css"
import VirtualTablePromotion from "../VirtualTable/VirtualTablePromotion";

function App() {
    const [isLoadClicked, setLoadClicked] = useState(false);
    const displayPromotionsTable = () => {
        setLoadClicked(true);
    }
    return (
        <>
            <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                <img src="https://www.moonactive.com/wp-content/uploads/2019/09/Gray_logo2.svg" alt=""/>
            </nav>
            <main className={"container-lg"}>
                <div>
                    <div className="bg-light p-3 rounded row">
                        <h1 className={"mb-2 mb-md-0 col-10"}>Promotions</h1>
                        <button onClick={displayPromotionsTable} className="btn btn-lg btn-dark col-2">Load</button>
                    </div>
                    <div id={"table"} className="mt-3">
                        {isLoadClicked && <VirtualTablePromotion/>}
                    </div>
                </div>
            </main>
        </>
    );
}

export default App;
